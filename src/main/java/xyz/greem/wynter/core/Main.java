package xyz.greem.wynter.core;

class Main {

    public static void main(String[] args) {
        if (args.length == 0) args = new String[]{""};

        try {
            new Wynter(args[0]);
        } catch (Exception e) {
            System.out.println("Wynter failed to start: " + e.getMessage());
            System.out.println();
            System.out.println();
            System.out.println("Cause: " + e.getCause().getMessage());
            System.out.print("Stack Trace: "); e.printStackTrace();
            System.out.println();
        }
    }

}
