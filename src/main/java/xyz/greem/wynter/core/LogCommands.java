package xyz.greem.wynter.core;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.User;

import javax.script.ScriptException;

class LogCommands {

        static void sendCommandsToDiscord(User commandSender, String commandExecuted) {

            EmbedBuilder embed = new EmbedBuilder()
                    .addField("Command Executed", commandExecuted, false)
                    .addField("User", commandSender.getName() + "#" + commandSender.getDiscriminator(), false)
                    .setColor(0x7000FB)
                    .setAuthor(commandSender.getName(), commandSender.getEffectiveAvatarUrl(), commandSender.getAvatarUrl());

            Wynter.instance.mainGuild.getTextChannelById("463898305693483008").sendMessage(embed.build()).queue();

        }

        static void sendCommandsToDiscord(User commandSender, String commandExecuted, ScriptException e) {

            String stackTrace = "";
            StringBuilder stringBuilder = new StringBuilder();

            for (StackTraceElement el : e.getStackTrace()) stringBuilder.append(el.toString()).append("\n");

            EmbedBuilder embed = new EmbedBuilder()
                    .addField("Command Executed", commandExecuted, false)
                    .addField("User", commandSender.getName() + "#" + commandSender.getDiscriminator(), false)
                    .setColor(0x7000FB)
                    .setAuthor(commandSender.getName(), commandSender.getEffectiveAvatarUrl(), commandSender.getAvatarUrl())
                    .addField("Error Cause", e.getCause().getMessage(), true)
                    .addField("Stacktrace", stringBuilder.toString(), true);

            Wynter.instance.mainGuild.getTextChannelById("463898305693483008").sendMessage(embed.build()).queue();
    }
}

